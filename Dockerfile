FROM node:lts-alpine

RUN mkdir /app
WORKDIR /app

COPY package*.json ./

COPY . ./

RUN npm install

EXPOSE 3000

ENV VITE_BASE_URL="https://usnc.dev-webdevep.ru/auth-back/api/v2"
ENV VITE_TOKEN_KEY="accessToken"

RUN npm run build

RUN npm install -g serve



CMD ["serve", "-s", "dist"]


