import { defineStore } from 'pinia';



export const useCommon = defineStore({
    id: 'common',
    state: () => ({
        pagesTitle: "",
        siderCollapsed: false
    }),
    actions: {
        setPagesTitle(value) {
            this.pagesTitle = value;
        },
        setSiderCollapsed() {
            this.siderCollapsed = !this.siderCollapsed;
        }
    },
});
