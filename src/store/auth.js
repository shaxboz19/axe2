import { defineStore } from 'pinia';
import {ref} from "vue";


export const useAuthStore = defineStore('auth', () => {
    const isAuth = ref(false);
    const userData = ref(null);

    function setAuth(value) {
        isAuth.value = value;
    }

    function setUserData(value) {
        userData.value = value;
    }

    return {
        isAuth,
        userData,
        setAuth,
        setUserData
    }
})
