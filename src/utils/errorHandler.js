export const errorHandler = (notification, error) => {
    const {
        data
    } = error.response
    notification.open({
        message: data.title,
        description: data.message,
        type: "error",

    });


}
