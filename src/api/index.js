import auth from './auth';
import contact from '@/api/contact';
import user from '@/api/user';
import { Axios } from 'axios';

export default ($axios) => ({
  auth: auth($axios),
  contact: contact($axios),
  user: user($axios),
});
