import {Axios} from "axios";

export default ($axios) => ({
    async getAll(params) {
        const data = await $axios.get("/contact", {
            params
        });
        return data;
    },
    async getById(id) {
        const {data} = await $axios.get(`/contact/${id}`);
        return data;
    },
    async create(form) {
        const data = await $axios.post("/contact", form);
        return data;
    },
    async update(id, form) {
        const data = await $axios.patch(`/contact/${id}`, form);
        return data;
    },
    async delete(id) {
        await $axios.delete(`/contact/${id}`);

    }

});
