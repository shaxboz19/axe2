export default ($axios) => ({
  async getUserInfo(form) {
    const { data } = await $axios.post('/api/getUserInfo/', form);
    return data;
  },
});
