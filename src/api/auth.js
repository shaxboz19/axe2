import { tokenService } from '@/utils';

export default ($axios) => ({
  // async login(form) {
  //   const { data } = await $axios.post('/auth/login/', form);
  //   const { access_token } = data;
  //   tokenService.saveToken(access_token);
  //   return data;
  // },
  async confrmEmail(form) {
    const { data } = await $axios.post('/api/sendConfirmCode', form);
    return data;
  },

  async register(form) {
    const { data } = await $axios.post('/api/register/', form);
    return data;
  },
  async logout() {
    tokenService.removeToken();
    return;
  },
});
