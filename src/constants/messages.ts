export const REQUIRED_FIELD = "This field is required";
export const INVALID_EMAIL = "Invalid email";

export const INVALID_PASSWORD = "Invalid password";
export const INVALID_USERNAME = "Invalid username";
export const INVALID_PHONE = "Invalid phone number";
