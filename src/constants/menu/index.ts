
const list = [
  {
    id: 1,
    title: "Contacts",
    icon: "menu/contact",
    path: "/contacts",
  },
  {
    id: 2,
    title: "User Info",
    icon: "menu/contact",
    path: "/user",
  },
];

export default list;
